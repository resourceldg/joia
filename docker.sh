#!/bin/bash

# Actualizar el sistema
sudo apt update

# Instalar dependencias para permitir el uso de repositorios a través de HTTPS
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

# Descargar la clave GPG oficial de Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Agregar el repositorio de Docker al sistema
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Actualizar el sistema nuevamente después de agregar el repositorio
sudo apt update

# Instalar Docker
sudo apt install -y docker-ce docker-ce-cli containerd.io

# Agregar el usuario actual al grupo "docker" para ejecutar comandos Docker sin sudo
sudo usermod -aG docker $USER

# Instalar Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Asignar permisos de ejecución a Docker Compose
sudo chmod +x /usr/local/bin/docker-compose

# Mostrar la versión de Docker y Docker Compose instalada
docker --version
docker-compose --version
